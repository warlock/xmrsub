from flask_login import LoginManager, login_user, login_required, current_user
from flask import Flask, redirect, render_template, jsonify, request
from .crypto import generate_secret_code
from .xmrsub import XmrSub
from .user import User

app = Flask(__name__, template_folder='templates')
xmrsub = XmrSub()

app.secret_key = generate_secret_code(32)
login_manager = LoginManager()
login_manager.init_app(app)

@login_manager.user_loader
def load_user(user_id) -> User | None:
    return xmrsub.get_user(user_id)

@app.route('/')
def on_main() -> str:
    xmrsub.update_prices()
    return render_template('index.html', **xmrsub.config)

@app.route('/dashboard')
@login_required
def on_dashboard() -> str:
    return render_template('dashboard.html', user=current_user)

@app.route('/api/v1/login', methods=['POST'])
def on_login():
    user_hash = request.args['hash']
    password = request.args['password']
    user = xmrsub.login_user(user_hash, password)
    if user:
        login_user(user)
        return redirect('/dashboard')
    return jsonify({'error' : 'incorrect data'})

@app.route('/api/v1/reg', methods=['POST'])
def on_reg():
    password = request.args['password']
    user = xmrsub.reg_user( password)
    if user:
        login_user(user)
        return redirect('/dashboard')
    return jsonify({'error' : 'incorrect data'})

@app.route('/api/v1/addPayment', methods=['POST'])
@login_required
def on_add_payment():
    amount = float(request.args['amount'])
    return jsonify({
        'address' : xmrsub.add_new_payment(current_user.user_hash, amount)
    })

@app.route('/api/v1/checkPayment', methods=['POST'])
@login_required
def on_check_payment():
    address = request.args['address']
    return jsonify({
        'isComplete' : xmrsub.check_payment(current_user.user_hash,address)
    })

if __name__ == '__main__':
    app.run('0.0.0.0', 4994, debug=True)


