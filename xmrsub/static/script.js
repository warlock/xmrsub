

var resultMsg = null;

var sendPOST = function(url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open('POST', url, true);
    xhr.onload = function() {
      var status = xhr.status;
      if (status === 200) {
        callback(null, xhr.response);
      } else {
        callback(status, xhr.response);
      }
    };
    xhr.send();
};

var getJSON = function(url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'json';
    xhr.onload = function() {
      var status = xhr.status;
      if (status === 200) {
        callback(null, xhr.response);
      } else {
        callback(status, xhr.response);
      }
    };
    xhr.send();
};

function onCopyAddressBtn(btn) {
    var address = btn.name;

    console.log(address);

    navigator.clipboard.writeText(address);
}

function onBuyBtn(btnId, parentId, priceId) {
    var info = document.createElement("p"); 
    info.className = "paymentCode";

    var btn = document.getElementById(btnId);
    var price = document.getElementById(priceId).innerHTML;
    price = price.replace(" XMR", "")

    var params = "price=" + price + "&itemId=" + parentId;
    getJSON('api/v1/addPayment?' + params,
    function(err, data) {
        if (err !== null) {
            alert('Something went wrong: ' + err);
        } else {
            var text = document.createTextNode(
                data.paymentCode
            );
            info.appendChild(text)
            btn.parentNode.replaceChild(info, btn); 
        }
    });
}

function onCheckPaymentBtn() {
    var code = document.getElementById("code").value;
    var txid = document.getElementById("txid").value;
    var par = document.getElementById("payment_checker");

    var params = "code=" + code + "&txid=" + txid;
    getJSON('api/v1/checkPayment?' + params,
    function(err, data) {
        if (err !== null) {
            alert('Something went wrong: ' + err);
        } else {
            var result = null;
            data = data.result;

            if (data.type == 'text') {
                var result = document.createElement("p");
                result.className = "resultText";
            } else if (data.type == 'url') {
                var result = document.createElement("a");
                result.href = data.result;
                result.className = "resultUrl";
            } else if (data.type == 'file') {
                var result = document.createElement("a");
                result.href = data.result;
                result.className = "resultFile";
            } else if (data.type == 'error') {
                var result = document.createElement("p");
                result.className = "resultError";
            }

            if (resultMsg != null) {
                resultMsg.parentNode.replaceChild(result, resultMsg); 
            }
            resultMsg = result;

            var text = document.createTextNode(
                data.result
            );

            result.appendChild(text);
            par.appendChild(result);
        }
    });
}

function onDashboardBtn() {
    getJSON('dashboard',
    function(err, data) {
        if (err !== null) {
            /*alert('Something went wrong: ' + err);*/
            $("#login").dialog({modal: true});
        } else {
            window.location.href = 'dashboard';
        }
    });
}

function onCreateNewAccBtn() {
    $("#login").dialog("close");
    $("#reg").dialog({modal: true});
}

function onLoginBtn() {
    var hash = document.getElementById("hash").value;
    var password = document.getElementById("login-password").value;

    sendPOST('api/v1/login?' + 'hash=' + hash + '&password=' + password, function(err, data) {
        if (err !== null) {
            alert('Something went wrong: ' + err);
        } else {
            window.location.href = 'dashboard';
        }
    });
}
 
function onRegBtn() {
    var password = document.getElementById("reg-password").value;
    sendPOST('api/v1/reg?password=' + password, function(err, data) {
        if (err !== null) {
            alert('Something went wrong: ' + err);
        } else {
            window.location.href = 'dashboard';
        }
    });
}

