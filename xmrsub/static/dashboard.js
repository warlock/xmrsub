

function onAddBalanceBtn() {
    $("#addbalance").dialog({
        modal: true,
        closeOnEscape: true,
        draggable: false,
        resizable: false
    });
}

function onNewPaymentBtn() {
    $("#addbalance").dialog("close");
    $("#waitforpayment").dialog({
        modal: true,
        closeOnEscape: true,
        draggable: false,
        resizable: false
    });

    var amount = document.getElementById("amount").value;

    sendPOST('api/v1/addPayment?amount=' + amount, function(err, data) {
        if (err !== null) {
            alert('Something went wrong: ' + err);
        } else {
            var address = data.address;
            if (address != null) {
                var str = document.getElementById("waitforpayment-str");
                var text = str.innerText;
                text = text.replace("$amount", amount);
                text = text.replace("$address", address);
                str.innerText = text;
            } else {
                $("#waitforpayment").dialog("close");
                alert("Can't connect to rpc!");
            }
        }
    });
}

function onPaymentCompleteBtn() {
    var address = "testaddress";

    sendPOST('api/v1/checkPayment?address=' + address, function(err, data) {
        if (err !== null) {
            alert('Something went wrong: ' + err);
        } else {
            var isComplete = data.result.isComplete;
            if (isComplete) {
                window.location.href = 'dashboard'; 
            } else {
                alert("Payment isn't complete!");
            }
        }
    });
}

function a() {
    var password = document.getElementById("reg-password").value;
    sendPOST('api/v1/reg?password=' + password, function(err, data) {
        if (err !== null) {
            alert('Something went wrong: ' + err);
        } else {
            window.location.href = 'dashboard';
        }
    });
}

