from werkzeug.security import generate_password_hash, check_password_hash
import secrets
import string

def generate_secret_code(size: int) -> str:
    seq = list()
    seq.extend(string.ascii_lowercase)
    seq.extend(string.ascii_uppercase)
    seq.extend(string.digits)

    code = ""
    for _ in range(size):
        code += secrets.choice(seq) 
    return code



