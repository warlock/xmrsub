import sqlite3
import time

class Database:

    def __init__(self) -> None:
        self.base = sqlite3.connect('data.db', check_same_thread=False)
        self.cursor = self.base.cursor()

        self.make_payments_table()

    def make_payments_table(self) -> None:
        self.cursor.execute("""CREATE TABLE IF NOT EXISTS users (
            hash TEXT,
            passwordHash TEXT,
            balance REAL
        )""")

        self.cursor.execute("""CREATE TABLE IF NOT EXISTS payments (
            hash TEXT,
            address TEXT,
            amount REAL,
            timestamp REAL,
            status TEXT
        )""")

        self.cursor.execute("""CREATE TABLE IF NOT EXISTS posts (
            hash TEXT,
            postId TEXT
            timestamp REAL,
            status TEXT
        )""")

        self.cursor.execute("""CREATE TABLE IF NOT EXISTS subscriptions (
            token TEXT,
            type TEXT,
            timestamp REAL
        )""")

        self.base.commit()

    def _insert(self, table: str, values: tuple) -> None:
        items = '?, ' * len(values)
        items = items[:-2]
        self.cursor.execute(f"INSERT INTO {table} VALUES ({items})", values)
        self.base.commit()

    def _delete(self, table: str, where: str, value: str) -> None:
        self.cursor.execute(f"DELETE FROM {table} WHERE {where} = (?)", (value,))
        self.base.commit()

    def _select_one(self, table: str, where: str, value: str | int) -> tuple | None:
        return self.cursor.execute(f"SELECT * FROM {table} WHERE {where} = (?)", (value,)).fetchone()

    def _select_all_where(self, table: str, where: str, value: str) -> list:
        return self.cursor.execute(f"SELECT * FROM {table} WHERE {where} = (?)", (value,)).fetchall()

    def _get_count(self, table: str) -> int:
        return len(self.cursor.execute(f"SELECT * FROM {table}").fetchall())

    def _select_all(self, table: str) -> list:
        return self.cursor.execute(f"SELECT * FROM {table}").fetchall()

    def is_user_exists(self, code: str) -> bool:
        return self._select_one('users', 'hash', code) != None

    def add_user(self, code: str, passwordHash: str) -> None:
        self._insert('users', (code, passwordHash, 0.))

    def get_user(self, code: str) -> tuple | None:
        return self._select_one('users', 'hash', code)

    def inc_user_balance(self, user_hash: str, balance: float) -> None:
        self.cursor.execute(
            "UPDATE users SET balance = balance + (?) WHERE hash = (?)",
            (balance, user_hash)
        )
        self.base.commit()

    def add_payment(self, user_hash: str, address: str, amount: float) -> None:
        self._insert('payments', (user_hash, address, amount, time.time(), 'wait'))

    def get_payment(self, address: str) -> tuple | None:
        return self._select_one('payments', 'address', address)

    def complete_payment(self, address: str) -> None:
        self.cursor.execute(
            "UPDATE payments SET status = (?) WHERE address = (?)",
            ('completed', address)
        )
        self.base.commit()

    def is_payment_complete(self, address: str) -> bool:
        return self.cursor.execute(
            "SELECT * FROM payments WHERE address = (?) AND status = (?)",
            (address, 'completed')
        ) != None

    def add_sub(self, code: str, sub_type: str) -> None:
        self._insert('subscriptions', (code, sub_type, time.time()))

    def add_post(self, user_hash: str, postId: str) -> None:
        return self._insert('posts', (user_hash, postId, time.time(), 'payed'))

    def is_post_payed(self, user_hash: str, postId: str) -> bool:
        return self.cursor.execute(
            "SELECT * FROM posts WHERE hash = (?) AND postId = (?) AND status = (?)",
            (user_hash, postId, 'payed')
        ) != None



