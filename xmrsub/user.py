from typing import Optional
from xmrsub.db import Database
from .crypto import *

class User:

    def __init__(self, user_hash: str, password_hash: str, balance: float):
        self.user_hash = user_hash
        self.password_hash = password_hash
        self.balance = balance

    def is_authenticated(self):
        return True
    
    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return str(self.user_hash)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password) -> bool:
        return check_password_hash(self.password_hash, password)

    def to_tuple(self) -> tuple:
        return (
            self.user_hash,
            self.password_hash,
            self.balance
        )

    @classmethod
    def from_tuple(cls, data: tuple):
        return cls(*data)

    @classmethod
    def from_db(cls, user_hash: str, db: Database) -> Optional["User"]:
        data = db.get_user(user_hash)
        if data: return User.from_tuple(data)
        else: return None

