import requests

class RPC:

    def __init__(self, url: str) -> None:
        self.url = url

    def request(self, method: str, params: dict) -> dict | None:
        try:
            resp = requests.post(self.url, json={
                "jsonrpc" : "2.0",
                "id" : "0",
                "method" : method,
                "params" : params, 
            }, headers={
                'content-type' : 'application/json'
            })
            return resp.json()
        except:
            pass

    def get_transfers(self) -> dict | None:
        return self.request(
            'get_transfers', 
            {'in' : True, 'account_index' : 0}
        )

    def create_address(self) -> str | None:
        result = self.request(
            'create_address',
            {'account_index' : 0}
        )
        if result:
            return result['result']['address']

    def get_transfers_old(self) -> dict | None:
        try:
            resp = requests.post(self.url, json={
                "jsonrpc" : "2.0",
                "id" : "0",
                "method" : "get_transfers",
                "params" : {
                    "in" : True,
                    "account_index" : 0
                }
            }, headers={
                'content-type' : 'application/json'
            })
            return resp.json()
        except:
            pass

