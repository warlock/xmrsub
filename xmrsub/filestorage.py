from subprocess import check_output 

class FileStorage:

    @staticmethod
    def load(filepath: str, config: dict) -> str | None:
        if config['filestorage']['type'] == 'lufi':
            data = config['filestorage'].copy()
            data.pop('type', None)
            return FileStorage.load_lufi(
                filepath=filepath,
                **data
            )

    @staticmethod
    def load_lufi(url: str, filepath: str, expiration: int, burn_after_download: bool) -> str:
        process = check_output(
            [
                'lufi-cli',
                '-s', f'{url}',
                '-u', f'{filepath}',
                '-e', f'{expiration}',
                '-b' if burn_after_download else ''
            ],
        )

        output = process.decode('utf-8')
        url_start_index = output.find('url: ')
        return output[
            url_start_index + 5 : output.find('\n', url_start_index) 
        ]





