from io import BytesIO
from typing import Optional
from PIL import Image
import qrcode
import base64
import json
import os

from xmrsub.user import User

from .moneroprice import MoneroPrice
from .filestorage import FileStorage
from .db import Database
from .crypto import *
from .rpc import *

def json_result(res_type: str, result: str | bool | None) -> dict:
    return {
        'type' : res_type,
        'result' : result
    }

def json_error(result: str) -> dict:
    return json_result('error', result)

class XmrSub:

    current_dir = os.path.dirname(__file__)

    def __init__(self):
        self.db = Database()
        self.config = json.load(open("config.json"))
        self.rpc = RPC(self.config['rpc']['url'])
        self.generate_qrcode()
        self.generate_xmr_url()
        self.update_prices()

    def reg_user(self, password: str) -> Optional[User]:
        user_hash = generate_secret_code(12)
        password_hash = generate_password_hash(password)
        self.db.add_user(user_hash, password_hash)
        return self.get_user(user_hash)

    def login_user(self, user_hash: str, password: str) -> Optional[User]:
        user = self.get_user(user_hash)
        if user and user.check_password(password):
            return user

    def get_user(self, user_hash: str) -> Optional[User]:
        return User.from_db(user_hash, self.db)

    def add_new_payment(self, user_hash: str, amount: float):
        address = self.rpc.create_address()
        if address:
            self.db.add_payment(user_hash, address, int(amount * 1e12))
            return address

    def add_new_sub(self, sub_type: str):
        code = generate_secret_code(32)
        self.db.add_sub(code, sub_type)
        return code

    def check_payment(self, user_hash: str, address: str):
        if self.db.is_payment_complete(address):
            return True

        transfers = self.rpc.get_transfers()
        if not transfers:
            return False
            #return json_error("Can't connect to Monero Wallet RPC")

        transfer = None
        for trans in transfers['result']['in']:
            if trans['address'] == address:
                transfer = trans
                break
        if not transfer: return False#return json_error("You send incorrect transaction ID!")

        if transfer['confirmations'] < 10: 
            return False
            #return json_error(f"Your transfer has {transfer['confirmations']}. Please wait for 10+.")

        payment = self.db.get_payment(address)
        if not payment: 
            return False
            #return json_error("You send incorrect payment code!")
        if payment[4] == 'completed': 
            return False
            #return json_error("This payment already completed!")

        if payment[2] <= transfer['amount']:
            #item = self.find_item_by_id(payment[1])
            #if not item:  
                #return json_error('Incorrect item id.')

            self.db.complete_payment(address)
            self.db.inc_user_balance(user_hash, transfer['amount'] / 1e12)

            #if item['type'] == 'text':
            #    return json_result('text', item['text'])
            #elif item['type'] == 'file':
            #    return json_result('file', FileStorage.load(
            #        f"resources/{item['filename']}",
            #        self.config
            #    ))
            #elif item['type'] == 'url':
            #    return json_result('url', item['url'])
            #elif item['type'] == 'sub':
            #    return json_result(
            #        'text',
            #        self.add_new_sub(
            #            item['type']
            #        )
            #    )

        return False
        #return json_error('You send incorrect amount!')

    def find_item_by_id(self, itemId: str) -> dict | None:
        for item in self.config['items']:
            if item['id'] == itemId:
                return item
        return None

    def generate_xmr_url(self) -> None:
        rec_name = self.config['author']['username']
        tx_desc = self.config['author']['tx_description']
        url = f"monero:{self.config['author']['address']}?recipient_name={rec_name}&tx_description={tx_desc}"
        self.config['author']['wallet_url'] = url

    def generate_qrcode(self):
        buffer = BytesIO()
        logo = Image.open(f'{self.current_dir}/static/monero-logo.png')
        logo = logo.convert('RGBA')

        basewidth = 196
        wpercent = (basewidth/float(logo.size[0]))
        hsize = int((float(logo.size[1])*float(wpercent)))
        logo = logo.resize((basewidth, hsize), Image.Resampling.LANCZOS)

        qr = qrcode.QRCode(
            error_correction=qrcode.constants.ERROR_CORRECT_H,
        )
        qr.add_data(self.config['author']['address'])
        qr.make(fit=True)

        img = qr.make_image(fill_color="orange", back_color=(40,40,40))
        img = img.convert('RGBA')

        pos = ((img.size[0] - logo.size[0]) // 2,
               (img.size[1] - logo.size[1]) // 2)
        img.paste(logo, pos)

        img.save(buffer, format="PNG")
        qr_code = f"data:image/png;base64,{base64.b64encode(buffer.getvalue()).decode()}"
        self.config['qrcode'] = qr_code

    def is_usd(self) -> bool:
        return self.config['price']['type'] == 'usd'

    def update_prices(self):
        xmr_usd_rate = MoneroPrice.get(self.config)
        for item in self.config['items']:
            if self.is_usd():
                item['price_str'] = f"${item['price']}"
                item['price_xmr'] = f"{round(item['price']/xmr_usd_rate, 8)}"
            else:
                item['price_str'] = f"${round(item['price']*xmr_usd_rate, 2)}"
                item['price_xmr'] = f"{item['price']}"


