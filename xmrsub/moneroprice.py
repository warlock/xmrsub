import requests
import time

class MoneroPrice:

    xmr_usd_rate = 0.0
    rate_check_timestamp = 0.0
    rate_check_periodic = 6000.0

    @staticmethod
    def get(config: dict) -> float | None:
        cur_ts = time.time()
        if cur_ts <= MoneroPrice.rate_check_timestamp + MoneroPrice.rate_check_periodic:
            return MoneroPrice.xmr_usd_rate
        else:
            MoneroPrice.rate_check_timestamp = cur_ts

        if config['price']['api'] == 'coingecko':
            MoneroPrice.xmr_usd_rate = MoneroPrice.coingecko()
            return MoneroPrice.xmr_usd_rate

    @staticmethod
    def coingecko() -> float | None:
        try:
            # Make a GET request to the CoinGecko API for XMR/USD exchange rate
            response = requests.get('https://api.coincap.io/v2/assets')

            # Check if the request was successful
            if response.status_code == 200:
                data = response.json()['data']
                for coin in data:
                    if coin['id'] == 'monero':
                        xmr_usd_rate = round(float(coin['priceUsd']), 2)
                        return xmr_usd_rate
            else:
                print("Error: Could not retrieve XMR/USD exchange rate")
        except:
            pass



