# xmrsub 

A simple Monero shop page. Supports files, text and urls selling.

## Installation

```sh
$ git clone https://codeberg.org/librehub/xmrsub
$ cd xmrsub
$ pip install .
$ python -m xmrsub
```

## Getting started

1. Create `resources` dir, which will be contain your files.

2. You need to create `config.json`:

```json
{
    "price" : {
        "type" : "usd",
        "api" : "coingecko"
    },
    "filestorage" : {
        "type" : "lufi",
        "url" : "lufistorage-url",
        "expiration" : 1,
        "burn_after_download" : true
    },
    "rpc" : {
        "url" : "MONERO_JSON_RPC_URL"
    },

    "author" : {
        "username" : "your_username",
        "description" : "your_description",
        "avatar" : "your_avatar_url",
        "address" : "your_xmr_address",
        "tx_description" : "your_transaction_description (monero wallet only)"
    },
    "items" : [
        {
            "id" : "item1",
            "price" : 1,
            "title" : "First item",
            "description" : "A first item which i sell",
            "type" : "file",
            "filename" : "filename in resources which will be downloaded to filestorage and display its url after purchase"
        },
        {
            "id" : "item2",
            "price" : 10,
            "title" : "Second item",
            "description" : "A second item which i sell",
            "type" : "text",
            "text" : "text, which will displayed after purchase"
        },
        {
            "id" : "item3",
            "price" : 100,
            "title" : "Third item",
            "description" : "A third item which i sell",
            "type" : "url",
            "url" : "url which will displayed after purchase"
        }
    ]
}
```

## Contacts

| Contact                                               | Description       |
| :---:                                                 | :---              |
| [`Matrix`](https://matrix.to/#/#librehub:matrix.org)  | Matrix server     |

## Support

You can support us [here](https://warlock.codeberg.page).


