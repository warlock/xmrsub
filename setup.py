from setuptools import setup 
import xmrsub 

VERSION = xmrsub.VERSION
DESCRIPTION = 'A simple Monero sub page.'

with open("README.md", "r") as ofile:
    LONG_DESCRIPTION = ofile.read()

packages = [
    'xmrsub', 'xmrsub.templates', 'xmrsub.static'
]

# Setting up
setup(
    url="https://codeberg.org/librehub/xmrsub",
    name="xmrsub",
    version=VERSION,
    author="loliconshik3",
    author_email="loliconshik3@gmail.com",
    description=DESCRIPTION,
    long_description_content_type="text/markdown",
    long_description=LONG_DESCRIPTION,
    packages=packages,
    include_package_data=True,
    install_requires=['flask', 'qrcode', 'pillow'], 
    keywords=['python', 'profile', 'html', 'css', 'profile page', 'page', 'site', 'personal page', 'monero', 'subscriptions'],
    classifiers=[
        "Development Status :: 1 - Planning",
        "Intended Audience :: Developers",
        "Programming Language :: Python :: 3",
        "Operating System :: Unix",
        "Operating System :: MacOS :: MacOS X",
        "Operating System :: Microsoft :: Windows"
    ]
)
